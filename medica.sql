/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Zrzut struktury tabela medica.lekarze
CREATE TABLE IF NOT EXISTS `lekarze` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa_lekarza` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Zrzucanie danych dla tabeli medica.lekarze: ~3 rows (około)
/*!40000 ALTER TABLE `lekarze` DISABLE KEYS */;
INSERT INTO `lekarze` (`ID`, `nazwa_lekarza`) VALUES
	(1, 'dr n med Jan Kowalski'),
	(2, 'dr med Anita Kowalczuk'),
	(3, 'dr n med Janusz Tracz');
/*!40000 ALTER TABLE `lekarze` ENABLE KEYS */;


-- Zrzut struktury tabela medica.lekarze_placowki
CREATE TABLE IF NOT EXISTS `lekarze_placowki` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `id_placowki` int(11) NOT NULL,
  `id_lekarza` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Zrzucanie danych dla tabeli medica.lekarze_placowki: ~6 rows (około)
/*!40000 ALTER TABLE `lekarze_placowki` DISABLE KEYS */;
INSERT INTO `lekarze_placowki` (`ID`, `id_placowki`, `id_lekarza`) VALUES
	(1, 1, 3),
	(2, 1, 2),
	(3, 2, 1),
	(4, 2, 2),
	(5, 3, 1),
	(6, 3, 3);
/*!40000 ALTER TABLE `lekarze_placowki` ENABLE KEYS */;


-- Zrzut struktury tabela medica.placowki
CREATE TABLE IF NOT EXISTS `placowki` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa_placowki` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Zrzucanie danych dla tabeli medica.placowki: ~3 rows (około)
/*!40000 ALTER TABLE `placowki` DISABLE KEYS */;
INSERT INTO `placowki` (`ID`, `nazwa_placowki`) VALUES
	(1, 'Gdańsk Oliwa'),
	(2, 'Sopot'),
	(3, 'Gdynia');
/*!40000 ALTER TABLE `placowki` ENABLE KEYS */;


-- Zrzut struktury tabela medica.poradnie
CREATE TABLE IF NOT EXISTS `poradnie` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `id_placowki` int(11) NOT NULL,
  `nazwa_poradni` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Zrzucanie danych dla tabeli medica.poradnie: ~6 rows (około)
/*!40000 ALTER TABLE `poradnie` DISABLE KEYS */;
INSERT INTO `poradnie` (`ID`, `id_placowki`, `nazwa_poradni`) VALUES
	(1, 1, 'Poradnia ginekologiczna'),
	(2, 1, 'Poradnia geriatryczna'),
	(3, 2, 'Poradnia ortopedyczna'),
	(4, 2, 'Poradnia tracheotyczna'),
	(5, 3, 'Poradnia pediatryczna'),
	(6, 3, 'Poradnia pneumologiczna');
/*!40000 ALTER TABLE `poradnie` ENABLE KEYS */;


-- Zrzut struktury tabela medica.uslugi
CREATE TABLE IF NOT EXISTS `uslugi` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa_uslugi` text NOT NULL,
  `id_placowka` text NOT NULL,
  `id_poradnia` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8
/*!50100 PARTITION BY KEY () */;

-- Zrzucanie danych dla tabeli medica.uslugi: ~4 rows (około)
/*!40000 ALTER TABLE `uslugi` DISABLE KEYS */;
INSERT INTO `uslugi` (`ID`, `nazwa_uslugi`, `id_placowka`, `id_poradnia`) VALUES
	(1, 'Obdukcja', '1', '1'),
	(2, 'Badanie', '2', '1'),
	(3, 'Badanie specjalistyczne płuc', '3', '1'),
	(4, 'Badanie', '2', '2');
/*!40000 ALTER TABLE `uslugi` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
